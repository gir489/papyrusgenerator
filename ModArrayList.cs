﻿public class ModArrayList : List<ModObject>
{
	public void Add(string mod, string miscMod, string modName)
	{
		ModObject modObject = new ModObject
		{
			Mod = mod,
			MiscMod = miscMod,
			ModName = modName
		};
		base.Add(modObject);
	}

	public string GetMod(int index)
	{
		return this[index].Mod ?? string.Empty;
	}

	public string GetMiscMod(int index)
	{
		return this[index].MiscMod ?? string.Empty;
	}

	public string GetModName(int index)
	{
		return this[index].ModName ?? string.Empty;
	}
}