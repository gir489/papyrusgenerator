*Barrel
_MR_mod_Barrel_B_1S_C|Standard Barrel (Classic)|0x000063E8|0x000063E3
_MR_mod_Barrel_B_1A_C|Automatic Barrel (Classic)|0x000063E7|0x000063E2
_MR_mod_Barrel_B_1S|Standard Barrel|0x00001742|0x00001739
_MR_mod_Barrel_B_1A|Automatic Barrel|0x00001749|0x0000174B
_MR_mod_Barrel_B_2S_C|Heavy Barrel (Classic)|0x000063EA|0x000063E5
_MR_mod_Barrel_B_2A_C|Automatic Heavy Barrel (Classic)|0x000063E9|0x000063E4
_MR_mod_Barrel_B_2S|Heavy Barrel|0x0000451A|0x0000173A
_MR_mod_Barrel_50_C|.50 Cal Barrel (Classic)|0x000063E6|0x000063E1
_MR_mod_Barrel_B_2A|Automatic Heavy Barrel|0x00003D60|0x0000174C
_MR_mod_Barrel_50|.50 Cal Barrel|0x00003D5B|0x00003D5D
_MR_mod_Barrel_G_A|Railgun: automatic|0x00008A88|0x00008A89
_MR_mod_Barrel_G_S|Railgun: semi-automatic|0x00008A91|0x00008A92
_MR_mod_Barrel_G_SS|Railgun: semi-automatic (forced)|0x00008A90|0x00008A93
*HeadPos
_MR_mod_HeadPos_Away|Away from sight|0x00002E39|0x00000000
_MR_mod_HeadPos_Close|Close to the sight|0x00002E38|0x00000000
_MR_mod_HeadPos_VeryClose|Very Close to the sight|0x00002E3C|0x00000000
*Legendary
_MR_mod_Legendary_0|None|0x00006B6B|0x00000000
_MR_mod_Legendary_1|Nimble|0x00006B5D|0x00000000
_MR_mod_Legendary_2|Enraging|0x00006B5E|0x00000000
_MR_mod_Legendary_3|Lucky Weapon|0x00006B5F|0x00000000
_MR_mod_Legendary_4|Wounding|0x00006B60|0x00000000
_MR_mod_Legendary_5|Penetrating|0x00006B61|0x00000000
_MR_mod_Legendary_6|Stalker's|0x00006B62|0x00000000
_MR_mod_Legendary_7|VATS Enhanced|0x00006B69|0x00000000
*Magazine
_MR_mod_mag_B_1|Standard Magazine|0x00001735|0x00001736
_MR_mod_mag_B_2|Extended Magazine|0x00001738|0x00001737
_MR_mod_mag_G|Magazine ER sub-caliber|0x00008A8D|0x00008A8E
_MR_mod_mag_HE|.50 Beowulf HE Magazine|0x0000BFE7|0x0000BFE8
*Material
_MR_mod_MATERIAL_11|Avenger|0x0000A17B|0x00000000
_MR_mod_MATERIAL_11s|Avenger (Worn)|0x0000A17C|0x00000000
_MR_mod_MATERIAL_02|Child of Shadows|0x00007B0E|0x00000000
_MR_mod_MATERIAL_02s|Child of Shadows (Worn)|0x00007B0A|0x00000000
_MR_mod_MATERIAL_03|Golden Dragon|0x00007B0F|0x00000000
_MR_mod_MATERIAL_03s|Golden Dragon (Worn)|0x00007B0B|0x00000000
_MR_mod_MATERIAL_09|Master of Depths|0x00005488|0x00000000
_MR_mod_MATERIAL_09s|Master of Depths (Worn)|0x0000548A|0x00000000
_MR_mod_MATERIAL_07|Night Demon|0x0000A11F|0x00000000
_MR_mod_MATERIAL_07s|Night Demon (Worn)|0x0000A120|0x00000000
_MR_mod_MATERIAL_05|Orange Bride|0x00007B11|0x00000000
_MR_mod_MATERIAL_05s|Orange Bride (Worn)|0x00007B0D|0x00000000
_MR_mod_MATERIAL_04|Red Baron|0x00007B10|0x00000000
_MR_mod_MATERIAL_04s|Red Baron (Worn)|0x00007B0C|0x00000000
_MR_mod_MATERIAL_08|Sandworm|0x0000CF29|0x00000000
_MR_mod_MATERIAL_08s|Sandworm (Worn)|0x0000CF28|0x00000000
_MR_mod_MATERIAL_10|Snake Charmer|0x00005489|0x00000000
_MR_mod_MATERIAL_10s|Snake Charmer (Worn)|0x0000548B|0x00000000
_MR_mod_MATERIAL_06|Swamp Beast|0x00009980|0x00000000
_MR_mod_MATERIAL_06s|Swamp Beast (Worn)|0x00009981|0x00000000
_MR_mod_MATERIAL_01|Warrior of the Wasteland|0x000063BF|0x00000000
_MR_mod_MATERIAL_01s|Warrior of the Wasteland (Worn)|0x000063C1|0x00000000
*Damage
_MR_mod_Modificator_-15|Damage - 15%|0x00004535|0x00000000
_MR_mod_Modificator_-30|Damage - 30%|0x00004536|0x00000000
_MR_mod_Modificator_-45|Damage - 45%|0x00004537|0x00000000
_MR_mod_Modificator_000|Damage + 0%|0x00004523|0x00000000
_MR_mod_Modificator_025|Damage + 25%|0x00004524|0x00000000
_MR_mod_Modificator_050|Damage + 50%|0x00004525|0x00000000
_MR_mod_Modificator_100|Damage + 100%|0x00004526|0x00000000
_MR_mod_Modificator_150|Damage + 150%|0x00004527|0x00000000
_MR_mod_Modificator_250|Damage + 250%|0x00004529|0x00000000
_MR_mod_Modificator_500|Damage + 500%|0x00004528|0x00000000
_MR_mod_Modificator_750|Damage + 750%|0x0000452A|0x00000000
_MR_mod_Modificator_999|Damage + 1000%|0x0000452B|0x00000000
*Receiver
_MR_mod_receiver_2|Improved Receiver|0x0000451F|0x00000000
_MR_mod_receiver_1|Standard Receiver|0x00001734|0x00001733
_MR_mod_receiver_3|Upgraded Receiver|0x0000451D|0x0000173C
_MR_mod_receiver_4|Calibrated Receiver|0x0000451E|0x0000173D
*Reticle
~TacticalCM
_MR_mod_Reticle_01b|Circle Blue|0x00007353|0x00000000
_MR_mod_Reticle_01g|Circle Green|0x00007333|0x00000000
_MR_mod_Reticle_01r|Circle Red|0x00007306|0x00000000
_MR_mod_Reticle_02b|Dot Blue|0x00007354|0x00000000
_MR_mod_Reticle_02g|Dot Green|0x00007332|0x00000000
_MR_mod_Reticle_02r|Dot Red|0x00007307|0x00000000
_MR_mod_Reticle_03b|Predator Blue|0x00007355|0x00000000
_MR_mod_Reticle_03g|Predator Green|0x00007334|0x00000000
_MR_mod_Reticle_03r|Predator Red|0x0000730A|0x00000000
_MR_mod_Reticle_04b|Cross Blue|0x00007356|0x00000000
_MR_mod_Reticle_04g|Cross Green|0x00007335|0x00000000
_MR_mod_Reticle_04r|Cross Red|0x00007317|0x00000000
_MR_mod_Reticle_05b|Danger Blue|0x00007357|0x00000000
_MR_mod_Reticle_05g|Danger Green|0x00007336|0x00000000
_MR_mod_Reticle_05r|Danger Red|0x00007318|0x00000000
_MR_mod_Reticle_06b|Grab Blue|0x00007358|0x00000000
_MR_mod_Reticle_06g|Grab Green|0x00007337|0x00000000
_MR_mod_Reticle_06r|Grab Red|0x00007319|0x00000000
_MR_mod_Reticle_07b|Line Blue|0x00007359|0x00000000
_MR_mod_Reticle_07g|Line Green|0x00007338|0x00000000
_MR_mod_Reticle_07r|Line Red|0x0000731A|0x00000000
_MR_mod_Reticle_08b|Mark Blue|0x0000735A|0x00000000
_MR_mod_Reticle_08g|Mark Green|0x00007339|0x00000000
_MR_mod_Reticle_08r|Mark Red|0x0000731B|0x00000000
~TacticalMR
_MR_mod_Reticle_09b|Moon Blue|0x0000735B|0x00000000
_MR_mod_Reticle_09g|Moon Green|0x0000733A|0x00000000
_MR_mod_Reticle_09r|Moon Red|0x0000731C|0x00000000
_MR_mod_Reticle_10b|Quartet Blue|0x0000735C|0x00000000
_MR_mod_Reticle_10g|Quartet Green|0x0000733B|0x00000000
_MR_mod_Reticle_10r|Quartet Red|0x0000731D|0x00000000
_MR_mod_Reticle_11b|Trinity Blue|0x0000735D|0x00000000
_MR_mod_Reticle_11g|Trinity Green|0x0000733C|0x00000000
_MR_mod_Reticle_11r|Trinity Red|0x0000731E|0x00000000
_MR_mod_Reticle_12b|Blades Blue|0x00005C37|0x00000000
_MR_mod_Reticle_12g|Blades Green|0x00005C38|0x00000000
_MR_mod_Reticle_12r|Blades Red|0x00005C39|0x00000000
_MR_mod_Reticle_13b|Romb Blue|0x00005C3A|0x00000000
_MR_mod_Reticle_13g|Romb Green|-0x00005C3B|0x00000000
_MR_mod_Reticle_13r|Romb Red|0x00005C3C|0x00000000
~Hybrid
_MR_mod_ReticleC_01|Reticle 01|0x0000B069|0x00000000
_MR_mod_ReticleC_02|Reticle 02|0x0000B07F|0x00000000
_MR_mod_ReticleC_03|Reticle 03|0x0000B080|0x00000000
_MR_mod_ReticleC_04|Reticle 04|0x0000B081|0x00000000
_MR_mod_ReticleC_05|Reticle 05|0x0000B082|0x00000000
_MR_mod_ReticleC_06|Reticle 06|0x0000B083|0x00000000
_MR_mod_ReticleC_07|Reticle 07|0x0000B068|0x00000000
_MR_mod_ReticleC_08|Reticle 08|0x0000B084|0x00000000
_MR_mod_ReticleC_09|Reticle 09|0x0000B085|0x00000000
_MR_mod_ReticleC_10|Reticle 10|0x0000B086|0x00000000
_MR_mod_ReticleC_11|Reticle 11|0x0000B087|0x00000000
~Optical
_MR_mod_ReticleS_01|Reticle - Cross|0x0000B835|0x00000000
_MR_mod_ReticleS_02|Reticle - Dot|0x0000B82D|0x00000000
_MR_mod_ReticleS_03|Reticle - Duplex|0x0000B82E|0x00000000
_MR_mod_ReticleS_04|Reticle - Mil-Dot|0x0000B82F|0x00000000
_MR_mod_ReticleS_05|Reticle - Modern|0x0000B830|0x00000000
_MR_mod_ReticleS_06|Reticle - Original|0x0000B831|0x00000000
_MR_mod_ReticleS_07|Reticle - Futuristic|0x0000B832|0x00000000
_MR_mod_ReticleS_08|Reticle - Rangefinder|0x0000B833|0x00000000
_MR_mod_ReticleS_09|Reticle - Russian|0x0000B834|0x00000000
~Holosight
_MR_mod_Reticle_Holo_01b|Reticle 1 Blue|0x0000C04F|0x00000000
_MR_mod_Reticle_Holo_01g|Reticle 1 Green|0x0000C050|0x00000000
_MR_mod_Reticle_Holo_01r|Reticle 1 Red|0x0000C051|0x00000000
_MR_mod_Reticle_Holo_02b|Reticle 2 Blue|0x0000C7FF|0x00000000
_MR_mod_Reticle_Holo_02g|Reticle 2 Green|0x0000C802|0x00000000
_MR_mod_Reticle_Holo_02r|Reticle 2 Red|0x0000C805|0x00000000
_MR_mod_Reticle_Holo_03b|Reticle 3 Blue|0x0000C800|0x00000000
_MR_mod_Reticle_Holo_03g|Reticle 3 Green|0x0000C803|0x00000000
_MR_mod_Reticle_Holo_03r|Reticle 3 Red|0x0000C806|0x00000000
_MR_mod_Reticle_Holo_04b|Reticle 4 Blue|0x0000C801|0x00000000
_MR_mod_Reticle_Holo_04g|Reticle 4 Green|0x0000C804|0x00000000
_MR_mod_Reticle_Holo_04r|Reticle 4 Red|0x0000C807|0x00000000
_MR_mod_Reticle_Holo_05b|Reticle 5 Blue|0x0000D757|0x00000000
_MR_mod_Reticle_Holo_05g|Reticle 5 Green|0x0000D759|0x00000000
_MR_mod_Reticle_Holo_05r|Reticle 5 Red|0x0000D75B|0x00000000
_MR_mod_Reticle_Holo_06b|Reticle 6 Blue|0x0000D758|0x00000000
_MR_mod_Reticle_Holo_06g|Reticle 6 Green|0x0000D75A|0x00000000
_MR_mod_Reticle_Holo_06r|Reticle 6 Red|0x0000D75C|0x00000000
*Sights
_MR_mod_Sight_mechanic|Mechanical Sights|0x00001747|0x00000000
_MR_mod_Sight_Tactical|Tactical Sights|0x0000CF1E|0x0000CF1C
_MR_mod_Sight_Holo|Holosight|0x0000C04C|0x0000C04A
_MR_mod_Sight_Combat|Hybrid Sights|0x00005471|0x00001746
_MR_mod_Sight_Scope|Optical Sights|0x0000A8BE|0x0000A8BD
*Stock
_MR_mod_Stock_1|Folded Stock|0x00001743|0x00000000
_MR_mod_Stock_2|Stock for sniper shooting|0x0000453B|0x00000000
_MR_mod_Stock_3|Stock for automatic shooting|0x0000453C|0x00000000
_MR_mod_Stock_4|Stock for combined actions|0x0000B82B|0x00000000
_MR_mod_Stock_5|Stock for combined actions (perfect fit)|0x000073A3|0x00000000
*Muzzle
_MR_mod_Suppressor_B_C_None|No muzzle attachment|0x000063F4|0x00000000
_MR_mod_Suppressor_B|Suppressor|0x0000268D|0x0000268C
_MR_mod_Suppressor_B_C|Suppressor|0x000063EC|0x000063EB
*TEST
_MR_mod_TEST_1|Test 1|0x0000BFDC|0x00000000
_MR_mod_TEST_2|Test 2|0x0000BFDE|0x00000000
_MR_mod_TEST_3|Test 3|0x0000BFDF|0x00000000
_MR_mod_TEST_4|Test 4|0x0000BFE0|0x00000000
_MR_mod_TEST_5|Test 5|0x0000BFE1|0x00000000
*Zoom
~Hybrid
_MR_mod_Zoom_Combat_11|Zoom - 1.1X|0x00005477|0x00000000
_MR_mod_Zoom_Combat_15|Zoom - 1.5X|0x00005478|0x00000000
_MR_mod_Zoom_Combat_19|Zoom - 1.9X|0x00005479|0x00000000
_MR_mod_Zoom_Combat_20|Zoom - 2X|0x00005474|0x00000000
_MR_mod_Zoom_Combat_20_Grid|Zoom - 2X  (Grid)|0x0000B0ED|0x00000000
_MR_mod_Zoom_Combat_30|Zoom - 3X|0x00005475|0x00000000
_MR_mod_Zoom_Combat_30_Grid|Zoom - 3X  (Grid)|0x0000B0EE|0x00000000
_MR_mod_Zoom_Combat_40|Zoom - 4X|0x00005476|0x00000000
_MR_mod_Zoom_Combat_40_Grid|Zoom - 4X (Grid)|0x0000B0EF|0x00000000
~Holosight
_MR_mod_Zoom_Holo_11|Zoom - 1.1X|0x0000C057|0x00000000
_MR_mod_Zoom_Holo_15|Zoom - 1.5X|0x0000C058|0x00000000
_MR_mod_Zoom_Holo_19|Zoom - 1.9X|0x0000C059|0x00000000
~Optical
_MR_mod_Zoom_Scope_02|Zoom - 2X|0x0000B06C|0x00000000
_MR_mod_Zoom_Scope_02_Original|Zoom - 2X Original|0x0000B070|0x00000000
_MR_mod_Zoom_Scope_04|Zoom - 4X|0x0000A8BF|0x00000000
_MR_mod_Zoom_Scope_04_Original|Zoom - 4X Original|0x0000B061|0x00000000
_MR_mod_Zoom_Scope_04_Original_NV|Zoom - 4X Original NV|0x0000B06F|0x00000000
_MR_mod_Zoom_Scope_06|Zoom - 6X|0x0000A8C0|0x00000000
_MR_mod_Zoom_Scope_06_Original|Zoom - 6X Original|0x0000B062|0x00000000
_MR_mod_Zoom_Scope_06_Original_NV|Zoom - 6X Original NV|0x0000B06E|0x00000000
_MR_mod_Zoom_Scope_08|Zoom - 8X|0x0000A8C1|0x00000000
_MR_mod_Zoom_Scope_10_Original|Zoom - 9X Original|0x0000B063|0x00000000
_MR_mod_Zoom_Scope_10_Original_NV|Zoom - 9X Original NV|0x0000B06D|0x00000000
~Tactical
_MR_mod_Zoom_Tactical_11|Zoom - 1.1X|0x0000CF1F|0x00000000
_MR_mod_Zoom_Tactical_15|Zoom - 1.5X|0x0000CF20|0x00000000
_MR_mod_Zoom_Tactical_19|Zoom - 1.9X|0x0000CF21|0x00000000