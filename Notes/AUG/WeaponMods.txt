~Receiver
mod_AUG_Receiver_1Standard|Receiver - Standard|0x00000FC5|0x000026E0
mod_AUG_Receiver_2Light|Receiver - Lightweight Materials|0x00001793|0x000017A1
mod_AUG_Receiver_3Heavy|Receiver - Heavy Duty Frame|0x00001794|0x000017A2
mod_AUG_Receiver_4Reinforced|Receiver - Reinforced Parts|0x00001795|0x000017A3
mod_AUG_Receiver_5Factory|Receiver - Factory Condition|0x00001796|0x000017A4
mod_AUG_Receiver_6Rapid|Receiver - Fast Cycling Mechanisms|0x00001797|0x000017A5
mod_AUG_Receiver_7Range|Receiver - Sealed Chamber|0x00001798|0x000017A6
mod_AUG_Receiver_8Damage|Receiver - Improved Components|0x00001799|0x000017A7
~Barrel
mod_AUG_Barrel_Para|Barrel - 350mm Para|0x00000FC0|0x000026D8
mod_AUG_Barrel_Carbine|Barrel - 407mm Carbine|0x000026AB|0x000026D5
mod_AUG_Barrel_FullA1|Barrel - 508mm Rifle|0x000026AC|0x000026D6
mod_AUG_Barrel_HBAR|Barrel - 621mm HBAR|0x000026AD|0x000026D7
~Foregrip
mod_AUG_Foregrip_A3_Short|Foregrip - A3 Short Grip|0x000026B0|0x000026DD
mod_AUG_Foregrip_A1|Foregrip - A1 Polymer|0x00000FC3|0x000026DB
mod_AUG_Foregrip_A3_Heavy|Foregrip - A3 Heavy Grip|0x000026B1|0x000026DC
~Magazine
mod_AUG_Magazine_10mm_HP|Magazine - 10mm Hollow Point|0x000026B5|0x000026DE
mod_AUG_Magazine_10mm_HV|Magazine - 10mm Stabilized High Velocity|0x00002EB7|0x00001779
mod_AUG_Magazine_10mm_Heavy|Magazine - 10mm Heavy Round|0x00002EB6|0x00001778
mod_AUG_Magazine_10mm_AP|Magazine - 10mm Armor Piercing|0x00002EB5|0x00001777
mod_AUG_Magazine_10mm_Incendiary|Magazine - 10mm Incendiary Rounds|0x00002EB8|0x0000177A
mod_AUG_Magazine_556_HP|Magazine - 5.56mm Hollow Point|0x000026F1|0x000026F3
mod_AUG_Magazine_556_HV|Magazine - 5.56mm High Velocity|0x00002EBA|0x0000177D
mod_AUG_Magazine_556_AP|Magazine - 5.56mm Armor Piercing|0x00002EB9|0x0000177B
mod_AUG_Magazine_556_DU|Magazine - 5.56mm Depleted Uranium|0x0000176F|0x0000177C
~Sight
mod_AUG_Scope_Iron|Sight - Iron sights|0x00000FCF|0x000026EB
mod_AUG_Scope_MicroT1|Sight - Micro T1 Reflex Sight|0x00000FD1|0x000026ED
mod_AUG_Scope_SRS02|Sight - SRS02 Reflex Sight|0x00000FD3|0x000026EE
mod_AUG_Scope_Kobra|Sight - Kobra Reflex Sight|0x00000FD0|0x000026EC
mod_AUG_Scope_EoTech|Sight - XPS-3 Holographic Sight|0x00000FCE|0x000026EA
mod_AUG_Scope_Bravo4|Sight - SIG Bravo4 Optical Sight|0x00000FD2|0x000026E9
mod_AUG_Scope_A1IntegratedSight|Sight - AUG A1 Optical Sight|0x000026B4|0x000026E6
mod_AUG_Scope_AccuPoint_6x|Scope - Trijicon AccuPower [6x]|0x00000FCC|0x000026E7
mod_AUG_Scope_Bender_10x|Scope - Schmidt & Bender PMII [10x]|0x00000FCD|0x000026E8
~Muzzle
mod_AUG_Muzzle_None|Muzzle - Standard Muzzle Brake|0x00000FC4|0x000026DF
mod_AUG_Muzzle_Brake_Heavy|Muzzle - Heavy Muzzle Brake|0x00002E96|0x00002E9C
mod_AUG_Muzzle_Suppressor_Light|Muzzle - Lightweight Suppressor|0x00002E92|0x00002E9F
mod_AUG_Muzzle_Suppressor_Covert|Muzzle - Covert Suppressor|0x00002E93|0x00002E9D
mod_AUG_Muzzle_Suppressor_Tactical|Muzzle - Tactical Suppressor|0x00002E94|0x00002EA0
mod_AUG_Muzzle_Suppressor_Heavy|Muzzle - Heavy Suppressor|0x00002E95|0x00002E9E
~Stock
mod_AUG_Stock_Standard|Stock - Standard Polymer|0x000026B7|0x000026F0
mod_AUG_Stock_Heavy|Stock - Heavy Stock|0x000026B8|0x000026EF
~Color
mod_AUG_Colour_Olive|Polymer Color - Olive|0x00002EA5|0x00002EB2
mod_AUG_Colour_Tan|Polymer Color - Tan|0x00002EA9|0x00002EB3
mod_AUG_Colour_Black|Polymer Color - Black|0x00002EA7|0x00002EB0
mod_AUG_Colour_White|Polymer Color - White|0x00002EAA|0x00002EB4
mod_AUG_Colour_Blue|Polymer Color - Blue|0x00002EA8|0x00002EB1
~Reticle
mod_AUG_Reticle_A|Reticle - Style A|0x00000FC7|0x000026E1
mod_AUG_Reticle_B|Reticle - Style B|0x00000FC8|0x000026E2
mod_AUG_Reticle_C|Reticle - Style C|0x00000FC9|0x000026E3
mod_AUG_Reticle_D|Reticle - Style D|0x00000FCA|0x000026E4
mod_AUG_Reticle_E|Reticle - Style E|0x00000FCB|0x000026E5
~FireMode
mod_AUG_FireMode_Automatic|Fire Mode - Automatic|0x00000FC1|0x000026D9
mod_AUG_FireMode_SemiAuto|Fire Mode - Semi-Automatic|0x000026AF|0x000026DA
~Rail
mod_AUG_Rail_None|Rail - None|0x00001780|0x0000178B
mod_AUG_Rail_Flashlight|Rail - Flashlight|0x00001781|0x00001789
mod_AUG_Rail_Laser|Rail - Laser Sight|0x00001782|0x0000178A
mod_AUG_Rail_Combo|Rail - Flashlight and Laser Sight Combo|0x00001783|0x00001788