*AQUILA
~Barrel
AQUILA.esp|OMOD|_mod_mgp_barrel_A|Sniper barrel|0x00000F9E|0x00000F9B
AQUILA.esp|OMOD|_mod_mgp_barrel_B|Heavy sniper barrel|0x00000F9D|0x00000F9C
AQUILA.esp|OMOD|_mod_mgp_barrel_C|Heavy Automatic Barrel|0x00001F45|0x00001F4F
AQUILA.esp|OMOD|_mod_mgp_barrel_D|Automatic Barrel|0x00003DF1|0x00003DF2
~Glow
AQUILA.esp|OMOD|_mod_mgp_GLOW_1|Glow - Red|0x00007AFB|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_2|Glow - Cyan|0x00007AFC|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_3|Glow - Green|0x00007AFD|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_4|Glow - Violet|0x00007B07|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_5|Glow - White|0x00007B08|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_6|Glow - Yellow|0x00007B09|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_7|Glow - Pink|0x00007B0A|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_8|Glow - Orange|0x00007B0B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_GLOW_9|Glow - Blue|0x00007B0C|0x00000000
~Grip
AQUILA.esp|OMOD|_mod_mgp_Grip|Sniper's butt|0x00000FA2|0x00000000
~Legendary
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Bloodied|Bloodied|0x00001F39|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Crippling|Crippling|0x00001F3A|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Criticals|Lucky Weapon|0x00001F38|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_NONE|None|0x000082C5|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Penetrating|Penetrating|0x00001F35|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Rapid|Rapid|0x00001F3B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Stability|Iron nerves|0x000082C3|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Staggering|Staggering|0x00001F37|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_VATS|VATS Enhanced|0x00001F3C|0x00000000
AQUILA.esp|OMOD|_mod_mgp_LEGENDARY_Wounding|Wounding|0x00001F36|0x00000000
~Magazine
AQUILA.esp|OMOD|_mod_mgp_Magazine1|Laser battery (Red)|0x00001EE5|0x00001EE3
AQUILA.esp|OMOD|_mod_mgp_Magazine2|Laser battery (Blue)|0x00001EE6|0x00001EE4
~Material
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_City|City on fire|0x00004D35|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_City_H|City on fire|0x00009201|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Dirty|Dirty|0x0000269A|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Dirty_H|Dirty|0x00009202|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Flame|Hexahedron|0x00006428|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Flame_H|Hexahedron|0x00009203|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Glow|Sci-Fi material|0x0000735D|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Hell|Hell fire|0x00002EAE|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Hell_H|Hell fire|0x00009204|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Jungle|Snake in the grass|0x00004D38|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Jungle_H|Snake in the grass|0x00009205|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Mirror|Broken glass|0x00002EAD|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Mirror_H|Broken glass|0x00009206|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Original|Futuristic Material|0x0000269C|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Original_H|Futuristic Material|0x00009207|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Trava|Hemp|0x00002710|0x00000000
AQUILA.esp|OMOD|_mod_mgp_MATERIAL_Trava_H|Hemp|0x00009208|0x00000000
~Damage
AQUILA.esp|OMOD|_mod_mgp_Modiier0|Damage: +0%|0x00001766|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier100|Damage: +100%|0x00001779|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier150|Damage: +150%|0x000054D3|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier200|Damage: +200%|0x000054D4|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier25|Damage: +25%|0x00001767|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier400|Damage: +400%|0x000054D5|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier50|Damage: +50%|0x00001768|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier75|Damage: +75%|0x00001778|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Modiier999|Damage: +1000%|0x000054D6|0x00000000
~Module
AQUILA.esp|OMOD|_mod_mgp_Module1|Basic Module|0x00001EE1|0x00003DF4
AQUILA.esp|OMOD|_mod_mgp_Module2|Gamma emitting|0x00001EEA|0x00003DF5
AQUILA.esp|OMOD|_mod_mgp_Module3|Ionized emitting|0x00001EEB|0x00003DF6
AQUILA.esp|OMOD|_mod_mgp_Module4|Modulated emitting|0x00001EEC|0x00003DF7
~Condenser
AQUILA.esp|OMOD|_mod_mgp_Receiver1|Original condenser|0x00000FA7|0x00000FA5
AQUILA.esp|OMOD|_mod_mgp_Receiver2|Boosted condenser|0x00000FA6|0x00000FA4
~Reticle
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_1|Reticle - Big Brother|0x000054DC|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_2|Reticle - American Dream|0x000054DE|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_3|Reticle - Little Brother|0x00005C80|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_4|Reticle - Dagger|0x00005C81|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_5|Reticle - Bulls-eye|0x00005C82|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_6|Reticle - Double Punch|0x00005C83|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_7|Reticle - Eye of God|0x00005C84|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_8|Reticle - Trinity|0x00005C85|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_2_9|Reticle - Spears|0x00005C86|0x00000000
~ReticleB
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Circle|Reticle - Blue Circle|0x00002694|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Cross|Reticle - Blue Cross|0x00001F28|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Danger|Reticle - Blue Danger|0x00001F29|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Dot|Reticle - Blue Dot|0x00002692|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Horizon|Reticle - Blue Horizon|0x00001F2A|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Peek|Reticle - Blue Peek|0x00001F2B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Predator|Reticle - Blue Predator|0x00002697|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Rad|Reticle - Blue Rad|0x00001F2C|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_B_Romb|Reticle - Blue Romb|0x00001F2D|0x00000000
~ReticleR
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Circle|Reticle - Red Circle|0x000026FE|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Cross|Reticle - Red Cross|0x000026FD|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Danger|Reticle - Red Danger|0x000026FF|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Dot|Reticle - Red Dot|0x00002700|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Horizon|Reticle - Red Horizon|0x00002701|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Peek|Reticle - Red Peek|0x00002702|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Predator|Reticle - Red Predator|0x00002703|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Rad|Reticle - Red Rad|0x00002704|0x00000000
AQUILA.esp|OMOD|_mod_mgp_RETICLE_R_Romb|Reticle - Red Romb|0x00002705|0x00000000
~Scope
AQUILA.esp|OMOD|_mod_mgp_Scope1|Reflex sight|0x00001746|0x00001743
AQUILA.esp|OMOD|_mod_mgp_Scope2|Tactical sight|0x00004591|0x00001745
AQUILA.esp|OMOD|_mod_mgp_Scope3_1|Optical sight (4X)|0x00001747|0x00001744
AQUILA.esp|OMOD|_mod_mgp_Scope3_1_NV|Optical sight (4X Night Vision)|0x00001769|0x0000176E
AQUILA.esp|OMOD|_mod_mgp_Scope3_2|Optical sight (6X)|0x0000176A|0x0000176C
AQUILA.esp|OMOD|_mod_mgp_Scope3_2NV|Optical sight (6X Night Vision)|0x00001770|0x0000176F
AQUILA.esp|OMOD|_mod_mgp_Scope3_3|Optical sight (10X)|0x0000176B|0x0000176D
AQUILA.esp|OMOD|_mod_mgp_Scope3_3NV|Optical sight (10X Night Vision)|0x00001776|0x00001775
~ScopeReticle
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Cross|Reticle - Cross|0x0000174B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Dot|Reticle - Dot|0x0000175B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Duplex|Reticle - Duplex|0x00001759|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_MilDot|Reticle - Mil Dot|0x0000174F|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Modern|Reticle - Modern|0x0000174A|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Original|Reticle - Original|0x00001757|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Rangefinder1|Reticle - Futuristic|0x0000174D|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Rangefinder2|Reticle - Rangefinder|0x0000174E|0x00000000
AQUILA.esp|OMOD|_mod_mgp_ScopeReticle_Russian|Reticle - Russian|0x00001742|0x00000000
~Zoom
AQUILA.esp|OMOD|_mod_mgp_Zoom_12|Zoom - 1.2X|0x0000268B|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Zoom_15|Zoom - 1.5X|0x0000268C|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Zoom_18|Zoom - 1.8X|0x0000268D|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Zoom_2|Zoom - 2X|0x00004595|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Zoom_3|Zoom - 3X|0x00004596|0x00000000
AQUILA.esp|OMOD|_mod_mgp_Zoom_4|Zoom - 4X|0x00004597|0x00000000
