
CabotHouse01|Cabot House
CambridgeDiner01|Cambridge Diner
CambridgePD01|Cambridge Police Station
CambridgePolymerLabs01|Cambridge Polymer Labs
CharlestownLaundry|Charlestown Laundry
SouthBoston25|Church
DmndStandsCodman01|Codman Residence
CambridgeCollegeAdminBuilding|College Admin Building
DmndStandsTaphouse01|Colonial Taphouse
CombatZone01|Combat Zone
Financial14|Commonwealth Bank
ConcordCivicAccess01|Concord Civic Access
ConcordSpeakeasy|Concord Speakeasy
DmndStandsCooke01|Cooke Residence
CorvegaAssemblyPlant01|Corvega Assembly Plant
NorthEndSalemStreetCottage|Cottage
CroupManor01|Croup Manor Basement
CustomHouseTower01|Custom House Tower

DBTechHighSchool01|D.B. Technical High School
BackBayTallOffice01|Dartmouth Professional Building
DmndRadio01|Diamond City Radio
DmndWaterfrontCrockersHouse01|Doc Crocker's House
DmndWaterfrontSunsHouse01|Doctor Sun's House
CharlestownDrugDen|Drug Den
DmndDugoutInn01|Dugout Inn
DunwichBorers01|Dunwich Borers
DmndEarlsHouse01|Earl Sterling's House
EastBostonPrep01|East Boston Preparatory School

DmndFallons01|Fallon's Basement
FallonsDepartmentStore01|Fallon's Department Store
FaneuilHall01|Faneuil Hall
FederalRationStockpile01|Federal Ration Stockpile
FensStreetSewer01|Fens Street Sewer
InstituteFEVlab|FEV Lab
RelayTowerInt09|Fishing Boat Cabin
FortHagen01|Fort Hagen
FortHagen02|Fort Hagen Command Center
FortStrong01|Fort Strong Armory
FourLeafFishpacking01|Four Leaf Fishpacking Plant
FraternalPost11501|Fraternal Post 115

GeneralAtomicsFactory01|General Atomics Factory
WaldenPond01|Gift Shop Basement
GraygardenHomestead01|Graygarden Homestead
GreenetechGenetics01|Greenetech Genetics
GreenetechGenetics02|Greenetech Genetics
DmndGreenhouse01|Greenhouse
GNN01|Gunners Plaza
GNN02|Gunners Plaza Basement
GwinnettBrewery02|Gwinnett Main Brewery
GwinnettBrewery01|Gwinnett Restaurant

HalluciGen01|HalluciGen, Inc.
Waterfront12|Harbormaster Hotel
HardwareTown01|Hardware Town
BobbisNewPlace01|Hawthorne Estate
DmndStandsHawthorne01|Hawthorne Residence
Financial07|Haymarket Mall
HestersRobotics01|Hester's Consumer Robotics
DanversHideout|Hideout
SouthBoston19|High School
EsplanadeChurch01|Holy Mission Congregation
GoodneighborHotelRexford|Hotel Rexford
TheaterHub360|Hub 360
HubrisComics01|Hubris Comics

InstituteAdvSystems|Institute Advanced Systems
InstituteBioScience|Institute BioScience
InstituteReactor|Institute Reactor
InstituteRobotics|Institute Robotics
InstituteSRB|Institute SRB
InstituteCave|Institute Sublevel 21-D
IrishPrideShipyard01|Irish Pride Industries Shipyard
JamaicaPlain01|Jamaica Plain Town Hall Basement
DmndJohnsHouse01|John and Cathy's House
DmndStandsKellogg01|Kellogg's House
CambridgeKendallHospital01|Kendall Hospital
DmndStandsLatimer01|Latimer Residence
LaytonTowers01|Layton Towers
ListeningPostBravo01|Listening Post Bravo
LongneckLukowskis01|Longneck Lukowski's Cannery

MahkraFishpacking01|Mahkra Fishpacking
MaldenCenter01|Malden Center
MaldenDrainage01|Malden Drainage
EsplanadeMansion01|Marlborough House
TheaterMassBayMedicalCenter01|Mass Bay Medical Center
TheaterMassBayMedicalCenter02|Mass Bay Medical Radiology
MassFusion01|Mass Fusion Building
MassFusion02|Mass Fusion Main Reactor
MassPikeTunnel01|Mass Pike Tunnel
MassStateHouse01|Massachusetts State House
NorthEndMeanPastries|Mean Pastries
MedTekResearch01|Med-Tek Research
MedTekResearch02|Med-Tek Sub-Levels
MedfordMemorial01|Medford Memorial Hospital
DBTechHighSchool02|Medical Center Station
DmndSurgeryBasement01|Mega Surgery Center
RelayTowerInt12|Military Bunker
MiltonGeneral01|Milton General Hospital
DmndMoeCroninsHouse01|Moe Cronin's House
RedRocket01|Mole Rat Den
CambridgeMonsignorPlaza01|Monsignor Plaza
ConcordMuseum01|Museum of Freedom
MuseumOfWitchcraft01|Museum of Witchcraft
DmndCitySurplus01New|Myrna's House

NahantOceanSociety01|Nahant Oceanological Society
NationalGuardTrainingYard03|National Guard Armory
NationalGuardTrainingYard02|National Guard Barracks
NationalGuardTrainingYard01|National Guard Recruitment Office
CambridgeEastCITRaiderCamp01|Office Building
Financial06|Old Corner Bookstore
OldGulletSinkhole01|Old Gullet Sinkhole
OldNorthChurch01|Old North Church
InstituteOldRobotics01|Old Robotics
GoodneighborOldStateHouse|Old State House
RelayTowerInt15A|Operations

Vault114|Park Street Station
GlowingSeaPOIDB06Int|Parking Garage
ParsonsState01|Parsons State Administration
ParsonsState03|Parsons State Basement
ParsonsState02|Parsons State Insane Asylum
Theater16PearwoodResidences01|Pearwood Residences
DmndPembrokesHouse01|Pembroke Residence
PickmanGallery01|Pickman Gallery
DmndChoiceChops01|Polly's House
PoseidonEnergy01|Poseidon Energy
PoseidonEnergy02|Poseidon Energy Sublevels
PoseidonReservoir01|Poseidon Reservoir
BeaconHillPub|Prost Bar
PrydwenHull02|Prydwen Command Deck
PrydwenHull01|Prydwen Main Deck
InstituteTunnel01|Public Works Maintenence Area
DmndPublick01|Publick Occurrences

QuincyPD01|Quincy Police Lock up
RailroadHQ01|Railroad HQ
RailroadHQEscapeTunnel|Railroad HQ Escape Tunnel
RelayTowerInt14|Reactor Level
RelayTowerInt15B|Reactor Level
RevereBeachStation01|Revere Beach Station
GorskiCabin01|Root Cellar
SanctuaryRosaHouse|Rosa Residence
Financial28|Ruined Skyscraper

SandyCovesHome01|Sandy Coves Convalescent Home
SaugusIronworks02|Saugus Blast Furnace
SaugusIronworks01|Saugus Ironworks
DmndSchoolhouse01|Schoolhouse
CambridgeScienceCenter01|Science Center Gift Shop
DmndScienceCenter01|Science! Center
Vault81Secret|Secret Vault 81
DmndSecurity01|Security Office
SentinelSite01|Sentinel Site Prescott
CovenantHQ01|Sewer
ShawHighSchool01|Shaw High School
BigJohnsSalvage01|Shelter
RelayTowerInt04|Shelter
DmndOutfieldShengsHouse01|Sheng Kawolski's House
DmndSolomonsHouse01|Solomon's House
SouthBostonPoliceDepartment01|South Boston Police Department
RelayTowerInt17|Storage
RelayTowerInt05|Storage Compartment
NHMFreightDepot01|Strongroom
NorthEndValentiStation|Subway Station
SuffolkCountyCharterSchool01|Suffolk County Charter School
SuperDuperMart01|Super Duper Mart
DmndSurgeryCellar01|Surgery Cellar

BeaconHillBostonBugle|The Boston Bugle
BostonCommon02|The Boylston Club
TheCastle01|The Castle Tunnels
TheBigDig01|The Dig
WarehouseIndustrialMachines|The Incredible Machines
ShamrockTaphouse01|The Shamrock Taphouse
Switchboard|The Switchboard
GoodneighborTheThirdRail|The Third Rail
ThicketExcavations01|Thicket Excavations
Theater27TickerTapeLounge|Ticker Tape Lounge
TiconderogaStation01|Ticonderoga
RelayTowerInt08|Train Car
TrinityChurch01|Trinity Church
TrinityTower01|Trinity Tower Mid Level

USSConstitution01|U.S.S. Constitution
CambridgeChurchGraveyard01|Union's Hope Cathedral
UniversityPoint02|University Credit Union
UniversityPoint01|University Point: Sedgwick Hall
USAFSatellite01|USAF Satellite Station Olivia
BunkerHill01|Utility Basement
DmndValentines01|Valentine Detective Agency
VirgilsLab01|Virgil's Laboratory

DmndWarehouseA01|Warehouse
GoodneighborWarehouse02|Warehouse
GoodneighborWarehouse01|Warehouse
GoodneighborWarehouse03|Warehouse
Waterfront02|Warehouse
WarrenTheater01|Warren Theater
Financial30|Water Street Apartments
Wattz01|Wattz Electronics
Financial13|Weatherby Investment Trust
WestRoxburyStation01|West Roxbury Station
AtomatoysCorporateHQ01|Wilson Atomatoys Corporate HQ
WRVRBroadcastCenter01|WRVR Broadcast Center
Yangtze01|Yangtze
