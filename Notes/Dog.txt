*Dog
~Bandana
ClothesDog_BandanaBlue|Blue Bandana
ClothesDog_BandanaGunnerCamo|Gunner's Camo Bandana
ClothesDog_BandanaGunnerGreen|Gunner's Green Bandana
ClothesDog_BandanaMonkey|Jangles Bandana
ClothesDog_BandanaLeopard|Leopard Print Bandana
ClothesDog_Bandana|Red Bandana
ClothesDog_BandanaSkull|Skull Bandana
ClothesDog_BandanaStarsNStripes|Stars and Stripes Bandana
ClothesDog_BandanaStripes|Striped Bandana
~Collar
ClothesDog_DogCollarChain|Chain Dog Collar
ClothesDog_DogCollar|Dog Collar
ClothesDog_DogCollarDouble|Double Dog Collar
ClothesDog_DogCollarReinforced|Reinforced Dog Collar
ClothesDog_DogCollarSpiked|Spiked Dog Collar
~Armor
ClothesDog_ArmorMedium|Dog Armor
ClothesDog_DogHeadHelmet|Dog Helmet
ClothesDog_ArmorHeavy|Heavy Dog Armor
ClothesDog_ArmorLight|Light Dog Armor
ClothesDog_DogHeadMuzzle|Spiked Muzzle