Arcadia 0x00006128
Aldersea Day Spa 0x0001F980
Atom's Spring 0x0003A382
Beaver Creek Lanes 0x00016E3B
Briney's Bait and Tackle 0x0005D36B
Brooke's Head Lighthouse 0x0000ECE6
Children of Atom Shrine 0x0004F8E2
Cliff's Edge Hotel 0x00016E38
>Vault 118 0x00037CE9
Cranberry Island Bog 0x0004FE1C
Cranberry Island Docks 0x0004FE1A
Cranberry Island Supply 0x0004FE1E
Dalton Farm 0x00038EBD
DiMA's Cache 0x0004855F
Eagle's Cove Tannery 0x0001536D
Eden Meadows Cinemas 0x0004EAB5
Echo Lake Lumber 0x0001FC98
Far Harbor 0x00005C7C
Fringe Cove Docks 0x00011A82
Glowing Grove 0x0005691C
Haddock Cove 0x000316B9
Harbor Grand Hotel 0x0002F40E
Horizon Flight 1207 0x00016E35
Huntress Island 0x000567C1
Kitteredge Pass 0x000258E4
Longfellow's Cabin 0x0004FD25
MS Azalea 0x000347D2
Nakano Residence 0x000034F7
National Park Campground 0x000247C0
National Park Visitor's Center 0x00009352
National Park HQ 0x0001046F
Northwood Ridge Quarry 0x030258DE
Oceanarium 0x0004FFA7
Old Pond House 0x0304FFA9
Pine Crest Cavern 0x000258E8
Pump Control 0x00037DD7
Radiant Crest Shrine 0x00056919
Rayburn Point 0x0004D6B9
Red Death Island 0x00048AC0
Rock Point Camp 0x0005422E
Ruined Church 0x00056915
Ruined Radio Tower 0x00056923
Southwest Harbor 0x000399CB
The Nucleus 0x000448E
>The Nucleus 0x0002B5ED
>Nucleus Command Center 0x00003AD2 0.0 64 0.0
>The Vessel 0x00040F48 26.9848 -200.7497 -81.5379 Z:0.0
Vim! Pop Factory 0x00016F97
Waves Crest Orphanage 0x000567BF
Wind Farm Maintenance 0x0002F40C
Zephyr Ridge Camp 0x000568B9

(DEBUG)
DLC03Adv017SpawnCall 0x0004FBFB
DLC03AliasCell 0x000575D8
XDLC03REHoldingCell 0x0004B68E
DLC03DungeonTemplate 0x00000807 128.0 -64.0 0.0
TestCory01 0x0000291C -516.5039 -456.2782 0.0 Z: 255.1484
TestTony 0x00034E07 419.5416 795.0218 0x0.0 Z:0.0
TestClaraCoast 0x00040D6A 223.29 1042.2832 0.0001 Z:193.2691
TestMadCoast 0x000018E8 216.7079 -1916.9552 -329.8013
Vault 118 Holding Cell 0x0005090A

DiMA Memory Locations:
Memory 1: 0000750D
Memory 2: 0001CBD3
Memory 3: 0001D157
Memory 4: 0001E6E6
Memory 5: 0001E9FB