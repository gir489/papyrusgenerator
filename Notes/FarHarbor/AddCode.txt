Form objectToGive = Game.GetFormFromFile(0x0005158D, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	ObjectReference objectRef = playerActor.PlaceAtMe(objectToGive)
	objectRef.AttachMod(mod_Legendary_Weapon_VATSMelee)
	playerActor.AddItem(objectRef)
endIf

Form objectToGive = Game.GetFormFromFile(0x0003A388, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	ObjectReference objectRef = playerActor.PlaceAtMe(objectToGive)
	ObjectMod attachment = Game.GetFormFromFile(0x0003A387, "DLCCoast.esm") as ObjectMod
	objectRef.AttachMod(attachment)
	playerActor.AddItem(objectRef)
endIf

Form objectToGive = Game.GetFormFromFile(0x000540ED, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, 10)
endIf

ObjectReference harvRef = Game.GetPlayer().PlaceAtMe(CheatTerminal_Fencebuster)
Game.GetPlayer().AddItem(harvRef)


Form objectToGive = Game.GetFormFromFile(0x0005158F, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	ObjectReference objectRef = playerActor.PlaceAtMe(objectToGive)
	objectRef.AttachMod(mod_Legendary_Weapon_TwoShot)
	playerActor.AddItem(objectRef)
endIf