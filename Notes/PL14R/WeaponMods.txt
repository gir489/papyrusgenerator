~Receiver
mod_PL14_Receiver_Standard|Receiver - Standard|0x00000F9E|0x00001814
mod_PL14_Receiver_Standard_Compact|Receiver - Compact|0x00001808|0x00001815
mod_PL14_Receiver_Light|Receiver - Lightweight|0x00001800|0x00001813
mod_PL14_Receiver_Heavy|Receiver - Heavy Frame|0x00001801|0x00001810
mod_PL14_Receiver_Improved|Receiver - Improved Mechanism|0x00001802|0x00001811
mod_PL14_Receiver_Improved_Compact|Receiver - Compact Improved Mechanism|0x00001807|0x00001812
mod_PL14_Receiver_Hardened|Receiver - Hardened|0x00001803|0x0000180E
mod_PL14_Receiver_Hardened_Compact|Receiver - Compact Hardened|0x00001806|0x0000180F
~Barrel
mod_PL14_Barrel_Light|Barrel - Light Barrel|0x0000267E|0x000017EC
mod_PL14_Barrel_Heavy|Barrel - Heavy Barrel|0x000017C6|0x000017EB
~Grip
mod_PL14_Grip_Standard|Grip - Standard Polymer|0x00000F9B|0x000017ED
mod_PL14_Grip_Wood|Grip - Wooden|0x000017C7|0x000017EE
~Magazine
mod_PL14_Mag_HollowPoint|Magazine - 15x Hollow Point|0x00000F9D|0x000017EF
mod_PL14_Mag_HighVelocity|Magazine - 15x High Velocity|0x00001FBE|0x00001FCE
mod_PL14_Mag_ArmorPiercing|Magazine - 15x Armor Piercing|0x00001FBD|0x00001FCC
mod_PL14_Mag_38|Magazine - 18x Low Caliber|0x00001FBF|0x00001FC9
mod_PL14_Mag_45|Magazine - 12x High Caliber|0x00001FC0|0x00001FCB
mod_PL14_Mag_44|Magazine - 8x Magnum|0x00001FC1|0x00001FCA
mod_PL14_Mag_flechette|Magazine - 15x Flechette Rounds|0x00001FC2|0x00001FCD
~Sights
mod_PL14_Scope_IronSights|Sight - Iron Sights|0x00000F9C|0x000017FA
mod_PL14_Scope_GlowSights_Green|Sight - Tritium Sights [Green]|0x000017C8|0x000017F7
mod_PL14_Scope_GlowSights_Yellow|Sight - Tritium Sights [Yellow]|0x000017CA|0x000017F9
mod_PL14_Scope_GlowSights_Red|Sight - Tritium Sights [Red]|0x000017C9|0x000017F8
mod_PL14_Scope_GlowSights_Blue|Sight - Tritium Sights [Blue]|0x000017CB|0x000017F6
mod_PL14_Scope_Docter|Sight - Docter-III Reflex Sight|0x00001FB4|0x00001FBC
mod_PL14_Scope_ACRO|Sight - ACRO Reflex Sight|0x00001FB5|0x00001FBA
mod_PL14_Scope_Aimpoint|Sight - AimpointCS Reflex Sight|0x00001FB6|0x00001FBB
~Muzzle
mod_PL14_Muzzle_None|Muzzle - None|0x00001EE4|0x000017F3
mod_PL14_Muzzle_Brake|Muzzle - Muzzle Brake|0x000017C0|0x000017F0
mod_PL14_Muzzle_Compensator|Muzzle - Compensator|0x000017C1|0x000017F2
mod_PL14_Muzzle_Suppressor|Muzzle - Suppressor|0x000017C3|0x000017F4
mod_PL14_Muzzle_CompactSuppressor|Muzzle - Compact Suppressor|0x000017C5|0x000017F1
~Underbarrel
mod_PL14_Underbarrel_None|Underbarrel - None|0x000027B|0x000027BD
mod_PL14_Underbarrel_Flashlight|Underbarrel - Flashlight|0x000027AB|0x000027B8
mod_PL14_Underbarrel_Laser_Red|Underbarrel - Laser Sight [Red]|0x000027AD|0x000027BB
mod_PL14_Underbarrel_Laser_Green|Underbarrel - Laser Sight [Green]|0x000027AF|0x000027BA
mod_PL14_Underbarrel_Laser_Yellow|Underbarrel - Laser Sight [Yellow]|0x000027AE|0x000027BC
mod_PL14_Underbarrel_Laser_Blue|Underbarrel - Laser Sight [Blue]|0x000027B0|0x000027B9
~Slide
mod_PL14_Slide_Standard|Slide - Standard|0x000017CD|0x000017FF
mod_PL14_Slide_Hardened|Slide - Hardened|0x000017CE|0x000017FC
mod_PL14_Slide_Copper|Slide - Copper Plated|0x000017D4|0x000017FB
mod_PL14_Slide_Nickel|Slide - Nickel Plated|0x000017D3|0x000017FD
mod_PL14_Slide_Silver|Slide - Silver Plated|0x000017D5|0x000017FE
~Reticle
-Dot
mod_PL14R_Reticle_Dot_Red|Reticle - Dot [Red]|0x00001754|0x0000179E
mod_PL14R_Reticle_Dot_Green|Reticle - Dot [Green]|0x00001749|0x0000179D
mod_PL14R_Reticle_Dot_Yellow|Reticle - Dot [Yellow]|0x0000175F|0x0000179F
mod_PL14R_Reticle_Dot_Blue|Reticle - Dot [Blue]|0x000026B4|0x0000179C
-CircleDot
mod_PL14R_Reticle_CircleDot_Red|Reticle - Circle Dot [Red]|0x00001753|0x0000179A
mod_PL14R_Reticle_CircleDot_Green|Reticle - Circle Dot [Green]|0x00001748|0x00001799
mod_PL14R_Reticle_CircleDot_Yellow|Reticle - Circle Dot [Yellow]|0x0000175E|0x0000179B
mod_PL14R_Reticle_CircleDot_Blue|Reticle - Circle Dot [Blue]|0x000026B3|0x00001798
-Plus
mod_PL14R_Reticle_Plus_Red|Reticle - Plus [Red]|0x00001757|0x000017AA
mod_PL14R_Reticle_Plus_Green|Reticle - Plus [Green]|0x0000174C|0x000017A9
mod_PL14R_Reticle_Plus_Yellow|Reticle - Plus [Yellow]|0x00001762|0x000017AB
mod_PL14R_Reticle_Plus_Blue|Reticle - Plus [Blue]|0x000026B7|0x000017A8
-Holo
mod_PL14R_Reticle_Holo_Red|Reticle - Holographic [Red]|0x00001755|0x000017A2
mod_PL14R_Reticle_Holo_Green|Reticle - Holographic [Green]|0x0000174A|0x000017A1
mod_PL14R_Reticle_Holo_Yellow|Reticle - Holographic [Yellow]|0x00001760|0x000017A3
mod_PL14R_Reticle_Holo_Blue|Reticle - Holographic [Blue]|0x000026B5|0x000017A0
-Tee
mod_PL14R_Reticle_Tee_Red|Reticle - T-Shape [Red]|0x00001759|0x000017B2
mod_PL14R_Reticle_Tee_Green|Reticle - T-Shape [Green]|0x0000174E|0x000017B1
mod_PL14R_Reticle_Tee_Yellow|Reticle - T-Shape [Yellow]|0x00001764|0x000017B3
mod_PL14R_Reticle_Tee_Blue|Reticle - T-Shape [Blue]|0x000026B9|0x000017B0
-LargeTee
mod_PL14R_Reticle_LargeTee_Red|Reticle - Large T-Shape [Red]|0x00001756|0x000017A6
mod_PL14R_Reticle_LargeTee_Green|Reticle - Large T-Shape [Green]|0x0000174B|0x000017A5
mod_PL14R_Reticle_LargeTee_Yellow|Reticle - Large T-Shape [Yellow]|0x00001761|0x000017A7
mod_PL14R_Reticle_LargeTee_Blue|Reticle - Large T-Shape [Blue]|0x000026B6|0x000017A4
-SeparatedCircle
mod_PL14R_Reticle_SeparatedCircle_Red|Reticle - Separated Circle [Red]|0x00001758|0x000017AE
mod_PL14R_Reticle_SeparatedCircle_Green|Reticle - Separated Circle [Green]|0x0000174D|0x000017AD
mod_PL14R_Reticle_SeparatedCircle_Yellow|Reticle - Separated Circle [Yellow]|0x00001763|0x000017AF
mod_PL14R_Reticle_SeparatedCircle_Blue|Reticle - Separated Circle [Blue]|0x000026B8|0x000017AC
-X
mod_PL14R_Reticle_X_Red|Reticle - Cross [Red]|0x0000175C|0x000017BE
mod_PL14R_Reticle_X_Green|Reticle - Cross [Green]|0x00001751|0x000017BD
mod_PL14R_Reticle_X_Yellow|Reticle - Cross [Yellow]|0x00001767|0x000017BF
mod_PL14R_Reticle_X_Blue|Reticle - Cross [Blue]|0x000026BC|0x000017BC
-ThreeDots
mod_PL14R_Reticle_ThreeDots_Red|Reticle - Three Dots [Red]|0x0000175A|0x000017B6
mod_PL14R_Reticle_ThreeDots_Green|Reticle - Three Dots [Green]|0x0000174F|0x000017B5
mod_PL14R_Reticle_ThreeDots_Yellow|Reticle - Three Dots [Yellow]|0x00001765|0x000017B7
mod_PL14R_Reticle_ThreeDots_Blue|Reticle - Three Dots [Blue]|0x000026BA|0x000017B4
-TriangleDot
mod_PL14R_Reticle_TriangleDot_Red|Reticle - Triangle Dot [Red]|0x0000175B|0x000017BA
mod_PL14R_Reticle_TriangleDot_Green|Reticle - Triangle Dot [Green]|0x00001750|0x000017B9
mod_PL14R_Reticle_TriangleDot_Yellow|Reticle - Triangle Dot [Yellow]|0x00001766|0x000017BB
mod_PL14R_Reticle_TriangleDot_Blue|Reticle - Triangle Dot [Blue]|0x000026BB|0x000017B8
-Circle
mod_PL14R_Reticle_Circle_Red|Reticle - Circle [Red]|0x00001752|0x00001796
mod_PL14R_Reticle_Circle_Green|Reticle - Circle [Green]|0x00001747|0x00001795
mod_PL14R_Reticle_Circle_Yellow|Reticle - Circle [Yellow]|0x0000175D|0x00001797
mod_PL14R_Reticle_Circle_Blue|Reticle - Circle [Blue]|0x000026B2|0x00001794
