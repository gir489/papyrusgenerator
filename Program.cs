﻿public class PapyrusGenerator
{
	private static readonly string FILE_LOCATION = @"D:\OneDrive\Stuff\Fallout 4\PapyrusGenerator\Notes\AQUILA\";
	private static readonly string FILE_NAME = "WeaponMods.txt";
	private static readonly string DLC_FILE = "AQUILA.esp";
	private static readonly bool IS_FILE_DLC = true;
	private static readonly bool DO_YAML = true;
	private static readonly bool IS_AMMO = FILE_NAME.ToLower().Contains("ammo");
	private static readonly bool IS_GENERIC_ITEM = FILE_NAME.ToLower().Contains("weapons") || FILE_NAME.ToLower().Contains("armor");
	private static readonly bool IS_AID = FILE_NAME.ToLower().Contains("aiditems");
	private static readonly bool IS_ATTACHMENT = !IS_AMMO && !IS_GENERIC_ITEM && !IS_AID;

	private static string itemName = "";
	private static string category = "";
	private static string subcategory = "";
	private static ModArrayList modNames = new ModArrayList();

	public static void WriteFunctionSectionForFO4(StreamWriter writer, int i)
	{
		string mod = modNames.GetMod(i);
		string miscMod = modNames.GetMiscMod(i);
		writer.WriteLine("ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()");
		writer.WriteLine("if ( grabbedRef )");
		writer.WriteLine("\tgrabbedRef.AttachMod(" + mod + ")");
		if (!string.IsNullOrWhiteSpace(miscMod))
		{
			writer.WriteLine("else");
			writer.WriteLine("\tGame.GetPlayer().AddItem(" + miscMod + ")");
		}
		writer.WriteLine("endIf");
	}

	public static void WriteFunctionSectionForDLC(StreamWriter writer, int i)
	{
		string mod = modNames.GetMod(i);
		string miscMod = modNames.GetMiscMod(i);
		if (IS_AMMO)
		{
			writer.WriteLine("Form formFromMod = Game.GetFormFromFile(" + mod + ",\"" + DLC_FILE + "\")");
			writer.WriteLine("if (formFromMod)");
			writer.WriteLine("\tGame.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())");
			writer.WriteLine("endIf");
		}
		else if (IS_AID)
		{
			writer.WriteLine("Form formFromMod = Game.GetFormFromFile(" + mod + ",\"" + DLC_FILE + "\")");
			writer.WriteLine("if (formFromMod)");
			writer.WriteLine("\tGame.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())");
			writer.WriteLine("endIf");
		}
		else if (IS_GENERIC_ITEM)
		{
			writer.WriteLine("Form formFromMod = Game.GetFormFromFile(" + mod + ",\"" + DLC_FILE + "\")");
			writer.WriteLine("if (formFromMod)");
			writer.WriteLine("\tGame.GetPlayer().AddItem(formFromMod)");
			writer.WriteLine("endIf");
		}
		else
		{
			if ("0x00000000".Equals(miscMod))
			{
				writer.WriteLine("Form modToApply = Game.GetFormFromFile(" + mod + ",\"" + DLC_FILE + "\")");
				writer.WriteLine("if ( modToApply )");
				writer.WriteLine("\tObjectReference grabbedRef = Game.GetPlayerGrabbedRef()");
				writer.WriteLine("\tif ( grabbedRef )");
				writer.WriteLine("\t\tgrabbedRef.AttachMod(modToApply as ObjectMod)");
				writer.WriteLine("\tendIf");
				writer.WriteLine("endIf");
			}
			else
			{
				writer.WriteLine("Form modToApply = Game.GetFormFromFile(" + mod + ",\"" + DLC_FILE + "\")");
				writer.WriteLine("if ( modToApply )");
				writer.WriteLine("\tForm miscMod = Game.GetFormFromFile(" + miscMod + ",\"" + DLC_FILE + "\")");
				writer.WriteLine("\tObjectReference grabbedRef = Game.GetPlayerGrabbedRef()");
				writer.WriteLine("\tif ( grabbedRef )");
				writer.WriteLine("\t\tgrabbedRef.AttachMod(modToApply as ObjectMod)");
				writer.WriteLine("\telseif ( miscMod )");
				writer.WriteLine("\t\tGame.GetPlayer().AddItem(miscMod)");
				writer.WriteLine("\tendIf");
				writer.WriteLine("endIf");
			}
		}
	}

	public static void DoFooterForFO4(StreamWriter writer)
	{
		foreach (var v in modNames)
		{
			writer.WriteLine("ObjectMod Property " + v.Mod + " Auto Const");
		}
		foreach (var v in modNames)
		{
			writer.WriteLine("MiscObject Property " + v.MiscMod + " Auto Const");
		}
	}

	public static void PrintFile()
	{
		string fileName = itemName + category + subcategory;

		if (fileName.Length > 31)
		{
			// Truncate the fileName to fit within the limit
			fileName = fileName.Substring(0, 31);
		}
		using (StreamWriter writer = new StreamWriter(Path.Combine(FILE_LOCATION, fileName + ".psc"), false))
		{
			writer.WriteLine(";BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment");
			writer.WriteLine("Scriptname Fragments:Terminals:" + fileName + " Extends Terminal Hidden Const");
			for (int i = 0; i < modNames.Count; i++)
			{
				string index = (i + 1).ToString("D2");
				writer.WriteLine();
				writer.WriteLine(";BEGIN FRAGMENT Fragment_Terminal_" + index);
				writer.WriteLine("Function Fragment_Terminal_" + index + "(ObjectReference akTerminalRef)");
				writer.WriteLine(";BEGIN CODE");
				if (IS_FILE_DLC)
				{
					WriteFunctionSectionForDLC(writer, i);
				}
				else
				{
					WriteFunctionSectionForFO4(writer, i);
				}
				writer.WriteLine(";END CODE");
				writer.WriteLine("EndFunction");
				writer.WriteLine(";END FRAGMENT");
			}
			writer.WriteLine();
			writer.WriteLine(";END FRAGMENT CODE - Do not edit anything between this and the begin comment");
			if (!IS_FILE_DLC)
			{
				DoFooterForFO4(writer);
			}
			if (IS_AMMO)
			{
				writer.WriteLine("GlobalVariable Property CheatTerminal_AmmoToGive Auto Const");
			}
			else if (IS_AID)
			{
				writer.WriteLine("GlobalVariable Property CheatTerminal_AidToGive Auto Const");
			}
		}
		Console.WriteLine("Completed " + fileName + ".psc");
	}

	public static void ParseModsFromFileFO4Regular(string[] split)
	{
		modNames.Add(split[0], split[2], split[3]);
	}

	public static void ParseModsForDLC(string[] split)
	{
		modNames.Add(split[split.Length - 2], split[split.Length - 1], split[3]);
	}

	public static void UpdateYAML(string searchPattern)
	{
		if (!DO_YAML)
		{
			return;
		}

		// Find the file that contains the searchPattern in its name
		string[] files = Directory.GetFiles(FILE_LOCATION, "*.yaml");
		string yamlPath = files.FirstOrDefault(file => file.Contains(searchPattern)) ?? throw new FileNotFoundException("YAML file not found.");

		var yaml = File.ReadAllLines(yamlPath).ToList();

		int itemIndex = 0;
		for (int i = 0; i < yaml.Count; i++)
		{
			if (yaml[i].Trim().StartsWith("- ItemText:") && itemIndex < modNames.Count)
			{
				// Update ItemText
				yaml[i + 2] = $"    Value: '{(modNames[itemIndex].ModName?.Replace("'", "''") ?? "")}'";

				// Update ResponseText
				if (IS_ATTACHMENT)
				{
					yaml[i + 5] = $"    Value: 'Added {(modNames[itemIndex].ModName?.Replace("'", "''") ?? "")}'";
				}
				else
				{
					yaml[i + 5] = $"    Value: ''";
				}

				// Move to the next modName item
				itemIndex++;
			}
		}

		File.WriteAllLines(yamlPath, yaml);

		Console.WriteLine("Completed " + Path.GetFileName(yamlPath));
	}

	public static void PrintPapyrusFilesAndUpateYAMLFiles()
	{
		PrintFile();
		string searchPattern = IS_ATTACHMENT ? $"{itemName}_Mods_{category}" : $"{itemName}_{category}";
		if (!string.IsNullOrEmpty(subcategory))
		{
			searchPattern += $"_{subcategory}";
		}
		UpdateYAML(searchPattern);
	}
	public static void Main(string[] args)
	{
		Console.WriteLine("Starting work on " + FILE_NAME);
		string filePath = Path.Combine(FILE_LOCATION, FILE_NAME);

		using (StreamReader sr = new StreamReader(filePath))
		{
			string? line;
			while ((line = sr.ReadLine()) != null)
			{
				string[] split = line.Split('|');
				if (split.Length == 1) // Detected categorization symbol
				{
					if (modNames.Count != 0)
					{
						PrintPapyrusFilesAndUpateYAMLFiles();
						modNames.Clear();
					}
					modNames = new ModArrayList();
					switch (line[0])
					{
						case '*':
							itemName = line.Replace("*", "");
							category = "";
							subcategory = "";
							break;
						case '~':
							category = line.Replace("~", "");
							subcategory = "";
							break;
						case '-':
							subcategory = line.Replace("-", "");
							break;
					}
				}
				else // Must be our item line
				{
					if (IS_FILE_DLC)
					{
						ParseModsForDLC(split);
					}
					else
					{
						ParseModsFromFileFO4Regular(split);
					}
				}
			}
		}

		if (modNames.Count != 0)
		{
			PrintPapyrusFilesAndUpateYAMLFiles();
			modNames.Clear();
		}
		Console.WriteLine("Finished work on " + FILE_NAME);
	}
}
