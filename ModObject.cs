﻿public class ModObject
{
	public string? Mod { get; set; }
	public string? MiscMod { get; set; }
	public string? ModName { get; set; }

	public override string? ToString() { return Mod ?? string.Empty; }
}